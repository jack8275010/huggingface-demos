transformers
datasets
tqdm
sklearn
numpy<=1.20.0
protobuf<4
torch==1.11.0
