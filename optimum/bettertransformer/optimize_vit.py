import transformers
from transformers import AutoFeatureExtractor, AutoModelForImageClassification

# 
model_id = "juliensimon/autotrain-food101-1471154050"
task_type = "image-classification"

model = AutoModelForImageClassification.from_pretrained(model_id)
feature_extractor = AutoFeatureExtractor.from_pretrained(model_id)
pipe = transformers.pipeline(task_type, model=model, feature_extractor=feature_extractor)

# Evaluate original model

import evaluate
from datasets import load_dataset

dataset_id = "food101"
data = load_dataset(dataset_id, split="validation[:25%]", num_proc=8)
print(data)
metric = evaluate.load("accuracy")
evaluator = evaluate.evaluator(task_type)


def evaluate_pipeline(pipeline):
    results = evaluator.compute(
        model_or_pipeline=pipeline,
        data=data,
        metric=metric,
        label_column="label",
        label_mapping=model.config.label2id,
    )
    return results


print("*** Original model")
classifier = transformers.pipeline(task_type, model_id, device=0)
results = evaluate_pipeline(classifier)
print(results)

print("*** BetterTransformer model")

from optimum.pipelines import pipeline

# Blog post: https://pytorch.org/blog/a-better-transformer-for-fast-transformer-encoder-inference/
# Supported model architectures: https://huggingface.co/docs/optimum/bettertransformer/overview

classifier_bt = pipeline(task_type, model_id, accelerator="bettertransformer", device=0)
results = evaluate_pipeline(classifier_bt)
print(results)
